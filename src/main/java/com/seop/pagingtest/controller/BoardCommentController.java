package com.seop.pagingtest.controller;

import com.seop.pagingtest.entity.BoardDocument;
import com.seop.pagingtest.model.BoardCommentItem;
import com.seop.pagingtest.model.BoardCommentRequest;
import com.seop.pagingtest.model.CommonResult;
import com.seop.pagingtest.model.ListResult;
import com.seop.pagingtest.service.BoardCommentService;
import com.seop.pagingtest.service.BoardDocumentService;
import com.seop.pagingtest.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시판 댓글관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-comment")
public class BoardCommentController {
    private final BoardCommentService boardCommentService;

    @ApiOperation(value = "댓글 등록")
    @PostMapping("/new/{boardId}")
    public CommonResult setBoardComment(@PathVariable long boardId, @RequestBody @Valid BoardCommentRequest request) {
        boardCommentService.setBoardComment(boardId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 리스트")
    @GetMapping("/list/{boardId}/page/{pageNum}")
    public ListResult<BoardCommentItem> getBoardComments(@PathVariable long boardId, @PathVariable int pageNum) {
        return ResponseService.getListResult(boardCommentService.getBoardComments(boardId, pageNum), true);
    }

    @ApiOperation(value = "좋아요 개수 수정")
    @PutMapping("/good-count/{commentId}")
    public CommonResult putGoodCount(@PathVariable long commentId) {
        boardCommentService.putGoodCount(commentId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "싫어요 개수 수정")
    @PutMapping("/bad-count/{commentId}")
    public CommonResult putBadCount(@PathVariable long commentId) {
        boardCommentService.putBadCount(commentId);

        return ResponseService.getSuccessResult();
    }
}

