package com.seop.pagingtest.controller;

import com.seop.pagingtest.model.BoardDocumentItem;
import com.seop.pagingtest.model.BoardDocumentRequest;
import com.seop.pagingtest.model.CommonResult;
import com.seop.pagingtest.model.ListResult;
import com.seop.pagingtest.service.BoardDocumentService;
import com.seop.pagingtest.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시판 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board-document")
public class BoardDocumentController {
    private final BoardDocumentService boardDocumentService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/new")
    public CommonResult setBoardDocument(@RequestBody @Valid BoardDocumentRequest request) {
        boardDocumentService.setBoardDocument(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "게시글 리스트")
    @GetMapping("/list/{pageNum}")
    public ListResult<BoardDocumentItem> getBoardDocument(@PathVariable int pageNum) {

        return ResponseService.getListResult(boardDocumentService.getBoardDocuments(pageNum), true);
    }

    // pageNum pageSize을 둘다 받는 getMapping
    /*@ApiOperation(value = "게시글 리스트")
    @GetMapping("/list/{pageNum}")
    public ListResult<BoardDocumentItem> getBoardDocument(@PathVariable int pageNum, @RequestParam("pageSize") int pageSize) {

        return ResponseService.getListResult(boardDocumentService.getBoardDocuments(pageNum, pageSize), true);
    }*/
}
