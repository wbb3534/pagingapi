package com.seop.pagingtest.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
