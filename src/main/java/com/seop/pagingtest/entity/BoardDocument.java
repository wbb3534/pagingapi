package com.seop.pagingtest.entity;

import com.seop.pagingtest.interfaces.CommonModelBuilder;
import com.seop.pagingtest.model.BoardDocumentRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocument {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "제목")
    @Column(nullable = false, length = 100)
    private String title;
    @ApiModelProperty(notes = "내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;
    @ApiModelProperty(notes = "작성자")
    @Column(nullable = false, length = 20)
    private String writerName;
    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(notes = "조회수")
    @Column(nullable = false)
    private Integer viewCount;

    private BoardDocument(BoardDocumentBuilder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.viewCount = builder.viewCount;
    }

    public static class BoardDocumentBuilder implements CommonModelBuilder<BoardDocument> {
        private final String title;
        private final String contents;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final Integer viewCount;
        public BoardDocumentBuilder(BoardDocumentRequest request) {
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.writerName = request.getWriterName();
            this.dateCreate = LocalDateTime.now();
            this.viewCount = 0;
        }
        @Override
        public BoardDocument build() {
            return new BoardDocument(this);
        }
    }

}
