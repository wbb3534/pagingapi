package com.seop.pagingtest.entity;

import com.seop.pagingtest.interfaces.CommonModelBuilder;
import com.seop.pagingtest.model.BoardCommentRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardComment {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "댓글 시퀀스")
    @Column(nullable = false)
    private Long boardId;
    @ApiModelProperty(notes = "작성자")
    @Column(nullable = false, length = 20)
    private String writerName;
    @ApiModelProperty(notes = "작성일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(notes = "내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;
    @ApiModelProperty(notes = "좋아요 개수")
    @Column(nullable = false)
    private Integer goodCount;
    @ApiModelProperty(notes = "싫어요 개수")
    @Column(nullable = false)
    private Integer badCount;

    public void putGoodCount() {
        this.goodCount += 1;
    }
    public void putBadCount() {
        this.badCount += 1;
    }
    private BoardComment(BoardCommentBuilder builder) {
        this.boardId = builder.boardId;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.contents = builder.contents;
        this.goodCount = builder.goodCount;
        this.badCount = builder.badCount;
    }

    public static class BoardCommentBuilder implements CommonModelBuilder<BoardComment> {
        private final Long boardId;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final String contents;
        private final Integer goodCount;
        private final Integer badCount;

        public BoardCommentBuilder(long boardId, BoardCommentRequest request) {
            this.boardId = boardId;
            this.writerName = request.getWriterName();
            this.dateCreate = LocalDateTime.now();
            this.contents = request.getContents();
            this.goodCount = 0;
            this.badCount = 0;
        }
        @Override
        public BoardComment build() {
            return new BoardComment(this);
        }
    }
}
