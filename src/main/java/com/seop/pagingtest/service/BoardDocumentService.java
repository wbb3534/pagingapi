package com.seop.pagingtest.service;

import com.seop.pagingtest.entity.BoardDocument;
import com.seop.pagingtest.exception.CMissingDataException;
import com.seop.pagingtest.model.BoardDocumentItem;
import com.seop.pagingtest.model.BoardDocumentRequest;
import com.seop.pagingtest.model.ListResult;
import com.seop.pagingtest.repository.BoardDocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardDocumentService {
    public final BoardDocumentRepository boardDocumentRepository;

    public void setBoardDocument(BoardDocumentRequest request) {
        BoardDocument addData = new BoardDocument.BoardDocumentBuilder(request).build();

        boardDocumentRepository.save(addData);
    }

    public ListResult<BoardDocumentItem> getBoardDocuments(int pageNum) {
        // 원본 리스트를 가져오는데 페이징을 하기 위해서 List가 아닌 Page로 가져온다
        // 페이징을 하기 위해서 몇페이지를 보여줄건지 한페이지에 몇개를 보여줄건지를 정해야한다.
        // 그래서 ListConvertService.getPageable(pageNum)을 이용해서 결정한다 지금은 한페이지에 10개씩
        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum));

        List<BoardDocumentItem> result = new LinkedList<>();
        // originList는 Page라서 List를 빼올려면 originList.getContent()를 해서 List만 빼온다.
        for (BoardDocument item : originList.getContent()) {
            result.add(new BoardDocumentItem.BoardDocumentItemBuilder(item).build());
        }
        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }

    // pageNum pageSize 둘다 받음
   /* public ListResult<BoardDocumentItem> getBoardDocuments(int pageNum, int pageSize) {

        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum, pageSize));

        List<BoardDocumentItem> result = new LinkedList<>();

        for (BoardDocument item : originList.getContent()) {
            result.add(new BoardDocumentItem.BoardDocumentItemBuilder(item).build());
        }
        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }*/
}
