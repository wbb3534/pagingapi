package com.seop.pagingtest.service;

import com.seop.pagingtest.entity.BoardComment;
import com.seop.pagingtest.entity.BoardDocument;
import com.seop.pagingtest.exception.CMissingDataException;
import com.seop.pagingtest.model.BoardCommentItem;
import com.seop.pagingtest.model.BoardCommentRequest;
import com.seop.pagingtest.model.ListResult;
import com.seop.pagingtest.repository.BoardCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardCommentService {
    private final BoardCommentRepository boardCommentRepository;

    public void setBoardComment(long boardId, BoardCommentRequest request) {

        BoardComment addData = new BoardComment.BoardCommentBuilder(boardId,request).build();

        boardCommentRepository.save(addData);
    }

    public ListResult<BoardCommentItem> getBoardComments(long boardId, int pageNum) {
        // List가아닌 Page로 받는 이유는 페이징을 하기 위해서 이며 boardCommentRepository에게 boradId와 pageNum 넘겨야함
        // ListConvertService.getPageable(pageNum, 8) 를통해 페이지넘버와 몇개씩 보여주실건지를 설정
        Page<BoardComment> originList = boardCommentRepository.findAllByBoardIdOrderByIdDesc(boardId, ListConvertService.getPageable(pageNum, 8));

        if (originList.isEmpty()) throw new CMissingDataException();

        List<BoardCommentItem> result = new LinkedList<>();

        for (BoardComment item : originList.getContent()) {
            result.add(new BoardCommentItem.BoardCommentItemBuilder(item).build());
        }
        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()

        );
    }

    public void putGoodCount(long id) {
        BoardComment originData = boardCommentRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putGoodCount();

        boardCommentRepository.save(originData);
    }

    public void putBadCount(long id) {
        BoardComment originData = boardCommentRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putBadCount();

        boardCommentRepository.save(originData);
    }
}
