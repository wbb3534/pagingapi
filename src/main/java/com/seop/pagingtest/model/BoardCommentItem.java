package com.seop.pagingtest.model;

import com.seop.pagingtest.entity.BoardComment;
import com.seop.pagingtest.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardCommentItem {
    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "작정자")
    private String writerName;
    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;
    @ApiModelProperty(notes = "내용")
    private String contents;
    @ApiModelProperty(notes = "좋아요 개수")
    private Integer goodCount;
    @ApiModelProperty(notes = "싫어요 개수")
    private Integer badCount;

    private BoardCommentItem(BoardCommentItemBuilder builder) {
        this.id = builder.id;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.contents = builder.contents;
        this.goodCount = builder.goodCount;
        this.badCount = builder.badCount;
    }
    public static class BoardCommentItemBuilder implements CommonModelBuilder<BoardCommentItem> {
        private final Long id;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final String contents;
        private final Integer goodCount;
        private final Integer badCount;

        public BoardCommentItemBuilder(BoardComment boardComment) {
            this.id = boardComment.getId();
            this.writerName = boardComment.getWriterName();
            this.dateCreate = boardComment.getDateCreate();
            this.contents = boardComment.getContents();
            this.goodCount = boardComment.getGoodCount();
            this.badCount = boardComment.getBadCount();
        }
        @Override
        public BoardCommentItem build() {
            return new BoardCommentItem(this);
        }
    }
}

