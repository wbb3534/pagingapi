package com.seop.pagingtest.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class BoardDocumentRequest {
    @ApiModelProperty(notes = "제목(2~100)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String title;
    @ApiModelProperty(notes = "내용(10 ~)", required = true)
    @NotNull
    @Length(min = 10)
    private String contents;
    @ApiModelProperty(notes = "작성자(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String writerName;

}
