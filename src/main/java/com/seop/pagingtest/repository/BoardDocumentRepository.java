package com.seop.pagingtest.repository;

import com.seop.pagingtest.entity.BoardDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {
}
