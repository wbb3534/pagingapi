package com.seop.pagingtest.repository;

import com.seop.pagingtest.entity.BoardComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardCommentRepository extends JpaRepository<BoardComment, Long> {
    Page<BoardComment> findAllByBoardIdOrderByIdDesc(long id, Pageable pageable);
}
